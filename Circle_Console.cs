﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/// <summary>
/// Missing header comments to maintain standards
/// </summary>



/// Purpose: The complete solution isn't available and the Program currently can't run
/// So It's not serving an intended purpose
/// 



/// Legibility and Style (Seperate Comments Have Been Placed Throughout the code blocks to indicate Proper Legibility and Style



/// Maintainability: It's a small Program so the maintainability should be straight forward











namespace RGBAssignment1
{
    class Program
    {
        static void Main(string[] args)
        {

            
            
            // The code to generate a menu should be put outside of the Main Method and called within Main to keep code clean and easier to read


            Circle circle = new Circle();

            int radius = 0;
            int menuOption = 0;

            radius = GetRadiusFromUserInput(radius);

            do
            {
                do
                {
                    Console.WriteLine("1. Get Circle Radius");
                    Console.WriteLine("2. Change Circle Radius");
                    Console.WriteLine("3. Get Circle Circumference");
                    Console.WriteLine("4. Get Circle Area");
                    Console.WriteLine("5. Exit");
                    Console.WriteLine();

                    try
                    {
                        menuOption = int.Parse(Console.ReadLine());
                    }
                    catch (Exception ex)
                    {

                        Console.WriteLine("Wrong option, please select from the menu:");
                        Console.WriteLine();
                    }


                } while (menuOption == 0 || menuOption < 0 || menuOption > 5);

                switch (menuOption)
                {
                    case 1:
                        radius = circle.GetRadius();
                        Console.WriteLine("the radius value is now: " + radius);
                        Console.WriteLine();
                        break;
                    case 2:

                        radius = GetRadiusFromUserInput(radius);
                        circle.SetRadius(radius);
                        Console.WriteLine("the radius value is now: " + radius);
                        Console.WriteLine();
                        break;
                    case 3:
                        Console.WriteLine("The circumference of the circle is: " + Math.Round(circle.GetCircumference(), 2));
                        Console.WriteLine();
                        break;
                    case 4:
                        Console.WriteLine("The Area of the circle is: " + Math.Round(circle.GetArea(), 2));
                        Console.WriteLine();
                        break;
                    case 5:
                        Environment.Exit(0);
                        break;
                    default:
                        break;
                }

            } while (menuOption > 0 || menuOption < 5);
        }


        /// <summary>
        /// It's lacking a comment to indicate what the method is doing
        /// 
        /// Code would be cleaner if the method was with the seperate  Circle Class File.
        /// </summary>
        /// <param name="radius"></param>
        /// <returns></returns>
        private static int GetRadiusFromUserInput(int radius)
        {
            do
            {
                Console.WriteLine("Please enter radius value: ");
                Console.WriteLine();

                try
                {
                    radius = int.Parse(Console.ReadLine());
                }
                catch (Exception ex)
                {

                    Console.WriteLine("Not the correct format, please insert a int greater than zero!");
                    Console.WriteLine();
                }

            } while (radius == 0);
            return radius;
        }


    }

}
