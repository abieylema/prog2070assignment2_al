﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using PROG2070Assign2_AL;

namespace PROG2070Assign2.Tests
{
    public class TriangleSolverTest
    {
        
        /// <summary>
        /// takes int 2,3,4 arguments and should return string thatsays it is a scalene triangle
        /// </summary>

        [Test]
        public static void Analyze234()
        {
            //Arrange
           
            
            int sideA=2;
            int sideB=3;
            int sideC=4;

            //Act

            TriangleSolver.Analyze(sideA, sideB, sideC);

            
            




            //Assert
            Assert.AreEqual("The Triangle Is Scalene", TriangleSolver.Analyze(sideA, sideB, sideC));
            
            


        }

        /// <summary>
        /// takes int 2,2,3 arguments and should return string that says it is its an isosceles triangle
        /// </summary>
        [Test]
        public static void Analyze223()
        {
            //Arrange


            int sideA = 2;
            int sideB = 2;
            int sideC = 3;

            //Act

            TriangleSolver.Analyze(sideA, sideB, sideC);







            //Assert
            Assert.AreEqual("The Triangle Is Isosceles", TriangleSolver.Analyze(sideA, sideB, sideC));




        }

        /// <summary>
        /// takes int 2,2,2 arguments and should return string that says it is its an equilateral triangle
        /// </summary>

        [Test]
        public static void Analyze222()
        {
            //Arrange


            int sideA = 2;
            int sideB = 2;
            int sideC = 2;

            //Act

            TriangleSolver.Analyze(sideA, sideB, sideC);







            //Assert
            Assert.AreEqual("The Triangle Is Equilateral", TriangleSolver.Analyze(sideA, sideB, sideC));




        }


        /// <summary>
        /// takes int 1,2,3 arguments and should return string that says 
        /// The sum of any two sides must be greater than the measure of the third side
        /// </summary>
        /// 

        [Test]
        public static void Analyze123()
        {
            //Arrange


            int sideA = 1;
            int sideB = 2;
            int sideC = 3;

            //Act

            TriangleSolver.Analyze(sideA, sideB, sideC);







            //Assert
            Assert.AreEqual("The sum of any two sides must be greater than the measure of the third side", TriangleSolver.Analyze(sideA, sideB, sideC));




        }


        /// <summary>
        /// takes int 1,1,2 arguments and should return string that says 
        /// The sum of any two sides must be greater than the measure of the third side
        /// </summary>
         

        [Test]
        public static void Analyze112()
        {
            //Arrange


            int sideA = 1;
            int sideB = 1;
            int sideC = 2;

            //Act

            TriangleSolver.Analyze(sideA, sideB, sideC);







            //Assert
            Assert.AreEqual("The sum of any two sides must be greater than the measure of the third side", TriangleSolver.Analyze(sideA, sideB, sideC));




        }


        /// <summary>
        /// takes int 2,1,3 arguments and should return string that says 
        /// The sum of any two sides must be greater than the measure of the third side
        /// </summary>
        

        [Test]
        public static void Analyze213()
        {
            //Arrange


            int sideA = 2;
            int sideB = 1;
            int sideC = 3;

            //Act

            TriangleSolver.Analyze(sideA, sideB, sideC);







            //Assert
            Assert.AreEqual("The sum of any two sides must be greater than the measure of the third side", TriangleSolver.Analyze(sideA, sideB, sideC));




        }

        /// <summary>
        /// takes int 0,0,0 arguments and should return string that says 
        /// Invalid Input, Please Pick A Number which is equal to or greater than 1
        /// </summary>


        [Test]
        public static void Analyze000()
        {
            //Arrange


            int sideA = 0;
            int sideB = 0;
            int sideC = 0;

            //Act

            TriangleSolver.Analyze(sideA, sideB, sideC);







            //Assert
            Assert.AreEqual("Invalid Input, Please Pick A Number which is equal to or greater than 1", TriangleSolver.Analyze(sideA, sideB, sideC));




        }

        /// <summary>
        /// takes int 0, 1, 1 arguments and should return string that says 
        /// Invalid Input, Please Pick A Number which is equal to or greater than 1
        /// </summary>

        [Test]
        public static void Analyze011()
        {
            //Arrange


            int sideA = 0;
            int sideB = 1;
            int sideC = 1;

            //Act

            TriangleSolver.Analyze(sideA, sideB, sideC);







            //Assert
            Assert.AreEqual("Invalid Input, Please Pick A Number which is equal to or greater than 1", TriangleSolver.Analyze(sideA, sideB, sideC));




        }

        /// <summary>
        /// takes int 0, 0, 1 arguments and should return string that says 
        /// Invalid Input, Please Pick A Number which is equal to or greater than 1
        /// </summary>

        [Test]
        public static void Analyze001()
        {
            //Arrange


            int sideA = 0;
            int sideB = 0;
            int sideC = 1;

            //Act

            TriangleSolver.Analyze(sideA, sideB, sideC);







            //Assert
            Assert.AreEqual("Invalid Input, Please Pick A Number which is equal to or greater than 1", TriangleSolver.Analyze(sideA, sideB, sideC));




        }
    }
}
