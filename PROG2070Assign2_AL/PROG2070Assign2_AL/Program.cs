﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/* PROG2070Assign2_AL
 * Version 1
 * Abiey-Lema, 2020.02.23: Created
*/

namespace PROG2070Assign2_AL
{
    class Program
    {
        static void Main(string[] args)
        {
            bool running = true;
            bool validInput = true;
            int sideA;
            int sideB;
            int sideC;




            int menuOption;

           

            //Menu

            while (running)
            {

                Console.Clear();

                Console.WriteLine("");
                menuOption = MainMenu();

                Console.WriteLine("You Have Selected Option: " + menuOption);

               



                if (menuOption == 1)
                {
                    Console.Clear();
                    do
                    {
                        Console.WriteLine("Please enter Triangle Length for Side A: ");

                        int.TryParse(Console.ReadLine(), out sideA);

                        try
                        {



                            if (sideA >= 1)
                            {
                                validInput = true;
                                Console.Clear();
                                break;
                            }
                            else
                            {

                                Console.WriteLine("Invalid Select, Please Pick A Number which is equal to or greater than 1");
                                validInput = false;

                                Console.ReadKey();
                                Console.Clear();
                            }

                        }
                        catch (Exception)
                        {

                            Console.WriteLine("Invalid Input, please try again");
                            validInput = false;

                            Console.ReadKey();
                            Console.Clear();
                        }



                    } while (!validInput);



                    do
                    {

                        Console.WriteLine("Please enter Triangle Length for Side B: ");

                        int.TryParse(Console.ReadLine(), out sideB);

                        try
                        {



                            if (sideB >= 1)
                            {
                                validInput = true;
                                Console.Clear();
                                break;
                            }
                            else
                            {

                                Console.WriteLine("Invalid Select, Please Pick A Number which is equal to or greater than 1");
                                validInput = false;

                                Console.ReadKey();
                                Console.Clear();
                            }

                        }
                        catch (Exception)
                        {

                            Console.WriteLine("Invalid Input, please try again");
                            validInput = false;

                            Console.ReadKey();
                            Console.Clear();
                        }



                    } while (!validInput);


                    do
                    {
                        Console.WriteLine("Please enter Triangle Length for Side C: ");

                        int.TryParse(Console.ReadLine(), out sideC);

                        try
                        {



                            if (sideC >= 1)
                            {
                                validInput = true;
                                Console.Clear();
                                break;
                            }
                            else
                            {

                                Console.WriteLine("Invalid Select, Please Pick A Number which is equal to or greater than 1");
                                validInput = false;

                                Console.ReadKey();
                                Console.Clear();
                            }

                        }
                        catch (Exception)
                        {

                            Console.WriteLine("Invalid Input, please try again");
                            validInput = false;

                            Console.ReadKey();
                            Console.Clear();
                        }

                        
                       




                    } while (!validInput);

                    Console.Clear();
                    TriangleSolver.Analyze(sideA, sideB, sideC);
                    Console.ReadKey();


                }

               

                if (menuOption == 2)
                {
                    Console.WriteLine("You Are About To Exit The Program! Goodbye!");
                    Console.ReadKey();
                    running = false;

                }








            }

           


        }

    

        // Main Menu method

        static int MainMenu()
        {
            bool validInput = false;
            int userMenuInput;


            do
            {


                Console.Clear();
                Console.WriteLine("Main Menu");
                Console.WriteLine("\t Menu Option 1: Enter Triangle Dimensions");              
                Console.WriteLine("\t Menu Option 2: Exit Program");
                Console.WriteLine("");

                Console.WriteLine("Please select a menu option");

                
                int.TryParse(Console.ReadLine(), out userMenuInput);


                try
                {



                    if ((userMenuInput == 1) || (userMenuInput == 2))
                    {
                        validInput = true;
                    }
                    else
                    {
                        Console.WriteLine("Invalid Select, Please Choose Between Option 1 or 2");
                    }

                }
                catch (Exception)
                {
                    Console.WriteLine("Invalid Input, please try again");
                }

                
                


            } while (!validInput);
            return userMenuInput;
            


        }
    }
}
