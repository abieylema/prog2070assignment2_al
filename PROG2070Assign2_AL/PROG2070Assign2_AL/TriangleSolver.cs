﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROG2070Assign2_AL
{
    public static class TriangleSolver
    {


        /// <summary>
        /// Method that takes 3 arguments and returns a string which tells you if it's a triangle, and the type of triangle it is. 
        /// 
        /// </summary>
        /// <param name="sideA"></param>
        /// <param name="sideB"></param>
        /// <param name="sideC"></param>
        /// <returns>string msg</returns>
        public static string Analyze(int sideA, int sideB, int sideC)
        {
            string msg = "";




                if (sideA < 1 | sideB < 1 | sideC < 1)
                {
                msg = "Invalid Input, Please Pick A Number which is equal to or greater than 1";

                }

                else if (sideA+sideB <= sideC | sideA + sideC <= sideB | sideC + sideB <= sideA)
                {
                    Console.WriteLine("The sum of any two sides must be greater than the measure of the third side");
                    msg = "The sum of any two sides must be greater than the measure of the third side";




                }

                

                else if (sideA==sideB & sideA==sideC)
                {

                
               
                Console.WriteLine("The Triangle Is Equilateral");
                msg = "The Triangle Is Equilateral";





                }

                else if (sideA == sideB | sideA == sideC | sideB == sideC)
                {
                
                
                Console.WriteLine("The Triangle Is Isosceles");
                msg = "The Triangle Is Isosceles";





            }

                else
                {
                
                

                Console.WriteLine("The Triangle Is Scalene");
                msg = "The Triangle Is Scalene";






            }






            return msg;

           


        }

       
    }
}
